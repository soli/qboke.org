<!--
date   : 2014-04-19 23:54:15
title  : 联系方式
slug   : contact
author : Soli
format : MarkdownExtra
tags   : [QBoke, About, Git, Markdown, Blog]
type   : page
excerpt: |
  QBoke 是一个轻量级的博客生成系统，基于 PHP、Markdown 和 git。你可以用 Markdownn 写博客，用 git 管理它们，然后用 QBoke 发布和展示它们。
-->

QBoke
=====

QBoke 是一个轻量级的博客生成系统，基于 PHP、Markdown 和 git。你可以用 Markdownn 写博客，用 git 管理它们，然后用 QBoke 发布和展示它们。

[详情请看这里](about.html)

联系方式
-------

如果你有什么意见或建议，或者发现了 QBoke 的问题，欢迎访问如下链接进行提交：

<https://bitbucket.org/soli/qboke/issues>

或者通过一下方式进行联系：

Email: <soli@cbug.org>

Twitter: <https://twitter.com/solicomo>
